import React, { useState } from "react";
import PropTypes from "prop-types";
import { useAsyncDebounce } from "react-table/dist/react-table.development";

function GlobalFilter({ filter, setFilter }) {
  const [value, setValue] = useState(filter);

  const onChange = useAsyncDebounce((value) => {
    setFilter(value || undefined);
  }, 1000);

  return (
    <div style={{ textAlign: "center", margin: "20px 0", fontWeight: "bold" }}>
      Search:{" "}
      <input
        style={{
          borderRadius: "8px",
          border: "2px solid #000",
          width: "300px",
        }}
        value={value || ""}
        onChange={(e) => {
          setValue(e.target.value);
          onChange(e.target.value);
        }}
      />
    </div>
  );
}

GlobalFilter.propTypes = {
  filter: PropTypes.string,
  setFilter: PropTypes.func,
};

export default GlobalFilter;
