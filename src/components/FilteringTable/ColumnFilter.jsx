import React from "react";
import PropTypes from "prop-types";

function ColumnFIlter({ column }) {
  const { filterValue, setFilter } = column;

  return (
    <div style={{ marginTop: "6px" }}>
      Search:{" "}
      <input
        style={{
          borderRadius: "8px",
          border: "2px solid #000",
          background: "white",
        }}
        value={filterValue || ""}
        onChange={(e) => setFilter(e.target.value)}
      />
    </div>
  );
}

ColumnFIlter.propTypes = {
  column: PropTypes.object,
};

export default ColumnFIlter;
