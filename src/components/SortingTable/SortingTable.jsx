import React, { useMemo } from "react";

import { useTable, useSortBy } from "react-table";
import { COLUMNS } from "./columns";
import table_data from "../../data/table_data.json";

function SortingTable() {
  const columns = useMemo(() => COLUMNS, []);
  const data = useMemo(() => table_data, []);

  const tableInstance = useTable(
    {
      columns,
      data,
    },
    useSortBy
  );

  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    footerGroups,
    rows,
    prepareRow,
  } = tableInstance;

  return (
    <table {...getTableProps()}>
      <thead>
        {headerGroups.map((headerGroup, index) => (
          <tr key={index} {...headerGroup.getHeaderGroupProps()}>
            {headerGroup.headers.map((column, index) => (
              <th
                key={index}
                {...column.getHeaderProps(column.getSortByToggleProps())}
              >
                {column.render("Header")}
                <span>
                  {column.isSorted ? (column.isSortedDesc ? " ⬆️" : " ⬇️") : ""}
                </span>
              </th>
            ))}
          </tr>
        ))}
      </thead>

      <tbody {...getTableBodyProps()}>
        {rows.map((row, index) => {
          prepareRow(row);
          return (
            <tr key={index} {...row.getRowProps()}>
              {row.cells.map((cell, index) => {
                return (
                  <td key={index} {...cell.getCellProps()}>
                    {cell.render("Cell")}
                  </td>
                );
              })}
            </tr>
          );
        })}
      </tbody>

      <tfoot>
        {footerGroups.map((footerGroup, index) => (
          <tr key={index} {...footerGroup.getFooterGroupProps()}>
            {footerGroup.headers.map((column, index) => (
              <th key={index} {...column.getFooterProps()}>
                {column.render("Footer")}
              </th>
            ))}
          </tr>
        ))}
      </tfoot>
    </table>
  );
}

export default SortingTable;
