import React from "react";
import { Link, NavLink } from "react-router-dom";

import "./Navbar.scss";

function Navbar() {
  return (
    <div className="navbar">
      <div className="container">
        <div className="nav-logo">
          <Link to="/">
            <h1>Advance Packages</h1>
          </Link>
        </div>

        <ul className="nav-links">
          <NavLink
            className={({ isActive }) =>
              isActive ? "nav-item active-main-nav" : "nav-item"
            }
            to="/"
          >
            Home
          </NavLink>
          <NavLink
            className={({ isActive }) =>
              isActive ? "nav-item active-main-nav" : "nav-item"
            }
            to="/table/basic"
          >
            Table
          </NavLink>
        </ul>
      </div>
    </div>
  );
}

export default Navbar;
