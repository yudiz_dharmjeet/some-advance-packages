import React from "react";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import {
  ColumnHidingTable,
  ColumnOrderTable,
  FilteringTable,
  Navbar,
  PaginationTable,
  RowSelectionTable,
  SortingTable,
  Table,
} from "./components";
import { Home, ReactTable } from "./pages";

function App() {
  return (
    <>
      <BrowserRouter>
        <Navbar />
        <Routes>
          <Route index element={<Home />} />
          <Route path="table" element={<ReactTable />}>
            <Route path="basic" element={<Table />} />
            <Route path="sorting" element={<SortingTable />} />
            <Route path="filtering" element={<FilteringTable />} />
            <Route path="pagination" element={<PaginationTable />} />
            <Route path="rowselection" element={<RowSelectionTable />} />
            <Route path="columnorder" element={<ColumnOrderTable />} />
            <Route path="columnhiding" element={<ColumnHidingTable />} />
          </Route>
        </Routes>
      </BrowserRouter>
    </>
  );
}

export default App;
