import React from "react";
import { Helmet } from "react-helmet";

import "./Home.scss";

function Home() {
  return (
    <div className="home">
      <Helmet>
        <meta name="home" content="Home Page" />
        <title>Advance Packages</title>
        <link rel="canonical" href="http://mysite.com/" />
      </Helmet>

      <h2>Welcome to the Collection of Projects</h2>
      <p>That made by advance packages</p>
    </div>
  );
}

export default Home;
