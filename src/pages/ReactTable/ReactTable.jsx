import React from "react";
import { NavLink, Outlet } from "react-router-dom";
import { Helmet } from "react-helmet";

import "./ReactTable.scss";

function ReactTable() {
  return (
    <div className="react-table">
      <Helmet>
        <meta name="table" content="All tables from react-table package" />
        <title>Table</title>
        <link rel="canonical" href="http://mysite.com/table" />
      </Helmet>

      <div className="container">
        <ul className="react-table-nav">
          <NavLink
            className={({ isActive }) =>
              isActive
                ? "react-table-nav-item active-table-nav"
                : "react-table-nav-item"
            }
            to="/table/basic"
          >
            BasicTable
          </NavLink>
          <NavLink
            className={({ isActive }) =>
              isActive
                ? "react-table-nav-item active-table-nav"
                : "react-table-nav-item"
            }
            to="/table/sorting"
          >
            Sorting Table
          </NavLink>
          <NavLink
            className={({ isActive }) =>
              isActive
                ? "react-table-nav-item active-table-nav"
                : "react-table-nav-item"
            }
            to="/table/filtering"
          >
            Filtering Table
          </NavLink>
          <NavLink
            className={({ isActive }) =>
              isActive
                ? "react-table-nav-item active-table-nav"
                : "react-table-nav-item"
            }
            to="/table/pagination"
          >
            Pagination Table
          </NavLink>
          <NavLink
            className={({ isActive }) =>
              isActive
                ? "react-table-nav-item active-table-nav"
                : "react-table-nav-item"
            }
            to="/table/rowselection"
          >
            Row Selection Table
          </NavLink>
          <NavLink
            className={({ isActive }) =>
              isActive
                ? "react-table-nav-item active-table-nav"
                : "react-table-nav-item"
            }
            to="/table/columnorder"
          >
            Column Order Table
          </NavLink>
          <NavLink
            className={({ isActive }) =>
              isActive
                ? "react-table-nav-item active-table-nav"
                : "react-table-nav-item"
            }
            to="/table/columnhiding"
          >
            Column Hiding Table
          </NavLink>
        </ul>
      </div>
      <Outlet />
    </div>
  );
}

export default ReactTable;
